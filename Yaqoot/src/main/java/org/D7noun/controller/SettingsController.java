package org.D7noun.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.D7noun.facade.SettingsFacade;
import org.D7noun.model.Settings;

@ManagedBean
@ViewScoped
public class SettingsController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private SettingsFacade settingsFacade;

	private Settings types = new Settings();

	@PostConstruct
	public void init() {
		types = settingsFacade.getValues("types");

	}

	public void save() {
		try {
			types = settingsFacade.save(types);
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage("Save Succeeded"));
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	/**
	 * 
	 * D7noun
	 * 
	 */

	public SettingsFacade getSettingsFacade() {
		return settingsFacade;
	}

	public void setSettingsFacade(SettingsFacade settingsFacade) {
		this.settingsFacade = settingsFacade;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the types
	 */
	public Settings getTypes() {
		return types;
	}

	/**
	 * @param types
	 *            the types to set
	 */
	public void setTypes(Settings types) {
		this.types = types;
	}

}