package org.D7noun.converter;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.D7noun.model.Bill;
import org.D7noun.view.BillBean;

@ManagedBean
public class BillConverter implements Converter {

	/**
	 * 
	 */
	@EJB
	private BillBean billBean;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			if (value != null && !value.isEmpty()) {
				return billBean.findById(Long.parseLong(value));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Bill bill = (Bill) value;
			return String.valueOf(bill.getId());
		}
		return null;
	}

}
