package org.D7noun.converter;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.D7noun.model.Buyer;
import org.D7noun.view.BuyerBean;

@ManagedBean
public class BuyerConverter implements Converter {

	/**
	 * 
	 */
	@EJB
	private BuyerBean buyerBean;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			if (value != null && !value.isEmpty()) {
				return buyerBean.findById(Long.parseLong(value));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Buyer buyer = (Buyer) value;
			return String.valueOf(buyer.getId());
		}
		return null;
	}

}
