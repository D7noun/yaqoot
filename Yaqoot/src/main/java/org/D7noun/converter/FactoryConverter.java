package org.D7noun.converter;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.D7noun.model.Factory;
import org.D7noun.view.FactoryBean;

@ManagedBean
public class FactoryConverter implements Converter {

	/**
	 * 
	 */
	@EJB
	private FactoryBean factoryBean;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			if (value != null && !value.isEmpty()) {
				return factoryBean.findById(Long.parseLong(value));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Factory factory = (Factory) value;
			return String.valueOf(factory.getId());
		}
		return null;
	}

}
