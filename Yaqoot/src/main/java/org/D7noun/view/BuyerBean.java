package org.D7noun.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.D7noun.model.Buyer;
import org.D7noun.model.Item;

/**
 * Backing bean for Buyer entities.
 * <p/>
 * This class provides CRUD functionality for all Buyer entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class BuyerBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ItemBean itemBean;
	private List<Item> buyerItems = new ArrayList<Item>();

	/*
	 * Support creating and retrieving Buyer entities
	 */

	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private Buyer buyer;

	public Buyer getBuyer() {
		return this.buyer;
	}

	public void setBuyer(Buyer buyer) {
		this.buyer = buyer;
	}

	@Inject
	private Conversation conversation;

	@PersistenceContext(unitName = "yaqoot-pu", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public String create() {

		this.conversation.begin();
		this.conversation.setTimeout(1800000L);
		return "create?faces-redirect=true";
	}

	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}

		if (this.id == null) {
			this.buyer = this.example;
		} else {
			this.buyer = findById(getId());
			buyerItems = itemBean.getAllItemsByBuyerId(getId());
		}
	}

	public Buyer findById(Long id) {

		return this.entityManager.find(Buyer.class, id);
	}

	/*
	 * Support updating and deleting Buyer entities
	 */

	public String update() {
		this.conversation.end();

		try {
			if (this.id == null) {
				this.entityManager.persist(this.buyer);
				return "search?faces-redirect=true";
			} else {
				this.entityManager.merge(this.buyer);
				return "create?faces-redirect=true&id=" + this.buyer.getId();
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	public String delete() {
		this.conversation.end();

		try {
			Buyer deletableEntity = findById(getId());

			this.entityManager.remove(deletableEntity);
			this.entityManager.flush();
			return "search?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	/*
	 * Support searching Buyer entities with pagination
	 */

	private int page;
	private long count;
	private List<Buyer> pageItems;

	private Buyer example = new Buyer();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public Buyer getExample() {
		return this.example;
	}

	public void setExample(Buyer example) {
		this.example = example;
	}

	public String search() {
		this.page = 0;
		return null;
	}

	public void paginate() {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<Buyer> root = countCriteria.from(Buyer.class);
		countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
		this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

		// Populate this.pageItems

		CriteriaQuery<Buyer> criteria = builder.createQuery(Buyer.class);
		root = criteria.from(Buyer.class);
		TypedQuery<Buyer> query = this.entityManager
				.createQuery(criteria.select(root).where(getSearchPredicates(root)));
		query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
		this.pageItems = query.getResultList();
	}

	private Predicate[] getSearchPredicates(Root<Buyer> root) {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		String name = this.example.getName();
		if (name != null && !"".equals(name)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("name")), '%' + name.toLowerCase() + '%'));
		}
		String phoneNumber = this.example.getPhoneNumber();
		if (phoneNumber != null && !"".equals(phoneNumber)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("phoneNumber")),
					'%' + phoneNumber.toLowerCase() + '%'));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}

	public List<Buyer> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	/*
	 * Support listing and POSTing back Buyer entities (e.g. from inside an
	 * HtmlSelectOneMenu)
	 */

	public List<Buyer> getAll() {

		CriteriaQuery<Buyer> criteria = this.entityManager.getCriteriaBuilder().createQuery(Buyer.class);
		return this.entityManager.createQuery(criteria.select(criteria.from(Buyer.class))).getResultList();
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private Buyer add = new Buyer();

	public Buyer getAdd() {
		return this.add;
	}

	public Buyer getAdded() {
		Buyer added = this.add;
		this.add = new Buyer();
		return added;
	}

	/**
	 * @return the buyerItems
	 */
	public List<Item> getBuyerItems() {
		return buyerItems;
	}

	/**
	 * @param buyerItems
	 *            the buyerItems to set
	 */
	public void setBuyerItems(List<Item> buyerItems) {
		this.buyerItems = buyerItems;
	}
}
