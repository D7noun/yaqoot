package org.D7noun.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.D7noun.model.Factory;

/**
 * Backing bean for Factory entities.
 * <p/>
 * This class provides CRUD functionality for all Factory entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class FactoryBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/*
	 * Support creating and retrieving Factory entities
	 */

	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private Factory factory;

	public Factory getFactory() {
		return this.factory;
	}

	public void setFactory(Factory factory) {
		this.factory = factory;
	}

	@Inject
	private Conversation conversation;

	@PersistenceContext(unitName = "yaqoot-pu", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public String create() {

		this.conversation.begin();
		this.conversation.setTimeout(1800000L);
		return "create?faces-redirect=true";
	}

	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}

		if (this.id == null) {
			this.factory = this.example;
		} else {
			this.factory = findById(getId());
		}
	}

	public Factory findById(Long id) {

		return this.entityManager.find(Factory.class, id);
	}

	/*
	 * Support updating and deleting Factory entities
	 */

	public String update() {
		this.conversation.end();

		try {
			if (this.id == null) {
				this.entityManager.persist(this.factory);
				return "search?faces-redirect=true";
			} else {
				this.entityManager.merge(this.factory);
				return "create?faces-redirect=true&id=" + this.factory.getId();
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	public String delete() {
		this.conversation.end();

		try {
			Factory deletableEntity = findById(getId());

			this.entityManager.remove(deletableEntity);
			this.entityManager.flush();
			return "search?faces-redirect=true";
		} catch (PersistenceException e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("This Factory Is Connected To Another Card"));
			return null;
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	/*
	 * Support searching Factory entities with pagination
	 */

	private int page;
	private long count;
	private List<Factory> pageItems;

	private Factory example = new Factory();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public Factory getExample() {
		return this.example;
	}

	public void setExample(Factory example) {
		this.example = example;
	}

	public String search() {
		this.page = 0;
		return null;
	}

	public void paginate() {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<Factory> root = countCriteria.from(Factory.class);
		countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
		this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

		// Populate this.pageItems

		CriteriaQuery<Factory> criteria = builder.createQuery(Factory.class);
		root = criteria.from(Factory.class);
		TypedQuery<Factory> query = this.entityManager
				.createQuery(criteria.select(root).where(getSearchPredicates(root)));
		query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
		this.pageItems = query.getResultList();
	}

	private Predicate[] getSearchPredicates(Root<Factory> root) {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		String name = this.example.getName();
		if (name != null && !"".equals(name)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("name")), '%' + name.toLowerCase() + '%'));
		}
		String phoneNumber = this.example.getPhoneNumber();
		if (phoneNumber != null && !"".equals(phoneNumber)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("phoneNumber")),
					'%' + phoneNumber.toLowerCase() + '%'));
		}
		String address = this.example.getAddress();
		if (address != null && !"".equals(address)) {
			predicatesList
					.add(builder.like(builder.lower(root.<String>get("address")), '%' + address.toLowerCase() + '%'));
		}
		String email = this.example.getEmail();
		if (email != null && !"".equals(email)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("email")), '%' + email.toLowerCase() + '%'));
		}
		String contact = this.example.getContact();
		if (contact != null && !"".equals(contact)) {
			predicatesList
					.add(builder.like(builder.lower(root.<String>get("contact")), '%' + contact.toLowerCase() + '%'));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}

	public List<Factory> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	/*
	 * Support listing and POSTing back Factory entities (e.g. from inside an
	 * HtmlSelectOneMenu)
	 */

	public List<Factory> getAll() {

		CriteriaQuery<Factory> criteria = this.entityManager.getCriteriaBuilder().createQuery(Factory.class);
		return this.entityManager.createQuery(criteria.select(criteria.from(Factory.class))).getResultList();
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private Factory add = new Factory();

	public Factory getAdd() {
		return this.add;
	}

	public Factory getAdded() {
		Factory added = this.add;
		this.add = new Factory();
		return added;
	}
}
