package org.D7noun.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.D7noun.model.Buyer;
import org.D7noun.model.Item;
import org.D7noun.model.Item.ItemState;
import org.D7noun.model.SellingCard;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;

/**
 * Backing bean for SellingCard entities.
 * <p/>
 * This class provides CRUD functionality for all SellingCard entities. It
 * focuses purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt>
 * for state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class SellingCardBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/*
	 * Support creating and retrieving SellingCard entities
	 */

	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private SellingCard sellingCard;

	public SellingCard getSellingCard() {
		return this.sellingCard;
	}

	public void setSellingCard(SellingCard sellingCard) {
		this.sellingCard = sellingCard;
	}

	@Inject
	private Conversation conversation;

	@PersistenceContext(unitName = "yaqoot-pu", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public String create() {

		this.conversation.begin();
		this.conversation.setTimeout(1800000L);
		return "create?faces-redirect=true";
	}

	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}

		if (this.id == null) {
			this.sellingCard = this.example;
		} else {
			this.sellingCard = findById(getId());
			sellingPrice = this.sellingCard.getSellingPrice();
			discountPrice = this.sellingCard.getDiscountPrice();
		}

		if (this.sellingCard != null) {
			itemsListModel = new DualListModel<>();
			List<Item> itemsAvailable = new ArrayList<Item>();
			itemsAvailable = itemBean.getAllAvailableItems();

			if (this.sellingCard.getItems() != null) {
				for (Item item : this.sellingCard.getItems()) {
					itemsListModel.getTarget().add(item);
				}
			}

			if (itemsAvailable != null) {
				itemsListModel.setSource(itemsAvailable);
			}
		}
	}

	public SellingCard findById(Long id) {

		return this.entityManager.find(SellingCard.class, id);
	}

	/*
	 * Support updating and deleting SellingCard entities
	 */

	public String update() {
		this.conversation.end();

		try {

			if (this.id == null) {
				this.sellingCard.setSellingPrice(sellingPrice);
				this.sellingCard.setDiscountPrice(discountPrice);
				this.entityManager.persist(this.sellingCard);
				for (Item item : itemsAdded) {
					this.sellingCard.getItems().add(item);
					item.setItemState(ItemState.Sold);
					item.setSellingCard(sellingCard);
					this.entityManager.merge(item);
				}
				for (Item item : itemsRemoved) {
					this.sellingCard.getItems().remove(item);
					item.setItemState(ItemState.Available);
					item.setSellingCard(null);
					this.entityManager.merge(item);
				}
				return "search?faces-redirect=true";

			} else {
				for (Item item : itemsAdded) {
					this.sellingCard.getItems().add(item);
					item.setItemState(ItemState.Sold);
					item.setSellingCard(sellingCard);
					this.entityManager.merge(item);
				}
				for (Item item : itemsRemoved) {
					this.sellingCard.getItems().remove(item);
					item.setItemState(ItemState.Available);
					item.setSellingCard(null);
					this.entityManager.merge(item);
				}
				this.sellingCard.setSellingPrice(sellingPrice);
				this.sellingCard.setDiscountPrice(discountPrice);
				this.entityManager.merge(this.sellingCard);
				return "create?faces-redirect=true&id=" + this.sellingCard.getId();
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	public String delete() {
		this.conversation.end();

		try {
			SellingCard deletableEntity = findById(getId());
			Iterator<Item> iterItems = deletableEntity.getItems().iterator();
			for (; iterItems.hasNext();) {
				Item nextInItems = iterItems.next();
				nextInItems.setSellingCard(null);
				iterItems.remove();
				this.entityManager.merge(nextInItems);
			}
			this.entityManager.remove(deletableEntity);
			this.entityManager.flush();
			return "search?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	/*
	 * Support searching SellingCard entities with pagination
	 */

	private int page;
	private long count;
	private List<SellingCard> pageItems;

	private SellingCard example = new SellingCard();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public SellingCard getExample() {
		return this.example;
	}

	public void setExample(SellingCard example) {
		this.example = example;
	}

	public String search() {
		this.page = 0;
		return null;
	}

	public void paginate() {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<SellingCard> root = countCriteria.from(SellingCard.class);
		countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
		this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

		// Populate this.pageItems

		CriteriaQuery<SellingCard> criteria = builder.createQuery(SellingCard.class);
		root = criteria.from(SellingCard.class);
		TypedQuery<SellingCard> query = this.entityManager
				.createQuery(criteria.select(root).where(getSearchPredicates(root)));
		query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
		this.pageItems = query.getResultList();
	}

	private Item searchedItem;

	private Predicate[] getSearchPredicates(Root<SellingCard> root) {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		Buyer buyer = this.example.getBuyer();
		if (buyer != null) {
			predicatesList.add(builder.equal(root.get("buyer"), buyer));
		}

		Date sellingDate = this.example.getSellingDate();
		if (sellingDate != null) {
			predicatesList.add(builder.equal(root.get("sellingDate"), sellingDate));
		}

		if (searchedItem != null) {
			Join<SellingCard, Item> join = root.join("items");
			Predicate predicate = builder.like(builder.lower(join.<String>get("barcode")),
					'%' + searchedItem.getBarcode() + '%');
			predicatesList.add(predicate);
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}

	public List<SellingCard> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	/*
	 * Support listing and POSTing back SellingCard entities (e.g. from inside
	 * an HtmlSelectOneMenu)
	 */

	public List<SellingCard> getAll() {

		CriteriaQuery<SellingCard> criteria = this.entityManager.getCriteriaBuilder().createQuery(SellingCard.class);
		return this.entityManager.createQuery(criteria.select(criteria.from(SellingCard.class))).getResultList();
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private SellingCard add = new SellingCard();

	public SellingCard getAdd() {
		return this.add;
	}

	public SellingCard getAdded() {
		SellingCard added = this.add;
		this.add = new SellingCard();
		return added;
	}

	/**
	 * @return the searchedItem
	 */
	public Item getSearchedItem() {
		return searchedItem;
	}

	/**
	 * @param searchedItem
	 *            the searchedItem to set
	 */
	public void setSearchedItem(Item searchedItem) {
		this.searchedItem = searchedItem;
	}

	/**
	 * 
	 * D7noun: Start Pick List
	 * 
	 **/

	private DualListModel<Item> itemsListModel;
	@EJB
	private ItemBean itemBean;

	private List<Item> itemsAdded = new ArrayList<Item>();
	private List<Item> itemsRemoved = new ArrayList<Item>();
	private double sellingPrice = 0;
	private double discountPrice = 0;

	public void onTransfer(TransferEvent event) {
		for (Object object : event.getItems()) {
			Item item = (Item) object;
			if (event.isAdd()) {
				itemsAdded.add(item);
				sellingPrice += item.getSellingPrice();
				discountPrice += item.getDiscountPrice();
				if (itemsRemoved.contains(item)) {
					itemsRemoved.remove(item);
				}
			}
			if (event.isRemove()) {
				itemsRemoved.add(item);
				sellingPrice -= item.getSellingPrice();
				discountPrice -= item.getDiscountPrice();
				if (itemsAdded.contains(item)) {
					itemsAdded.remove(item);
				}
			}
		}
	}

	public DualListModel<Item> getItemsListModel() {
		return itemsListModel;
	}

	public void setItemsListModel(DualListModel<Item> itemsListModel) {
		this.itemsListModel = itemsListModel;
	}

	/**
	 * @return the sellingPrice
	 */
	public double getSellingPrice() {
		return sellingPrice;
	}

	/**
	 * @param sellingPrice
	 *            the sellingPrice to set
	 */
	public void setSellingPrice(double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	/**
	 * @return the discountPrice
	 */
	public double getDiscountPrice() {
		return discountPrice;
	}

	/**
	 * @param discountPrice
	 *            the discountPrice to set
	 */
	public void setDiscountPrice(double discountPrice) {
		this.discountPrice = discountPrice;
	}

	/**
	 * 
	 * D7noun: End Pick List
	 * 
	 **/

}