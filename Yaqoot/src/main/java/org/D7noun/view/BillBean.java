package org.D7noun.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.D7noun.facade.BillAttachmentFacade;
import org.D7noun.model.Bill;
import org.D7noun.model.BillAttachment;
import org.D7noun.model.Factory;
import org.D7noun.model.Item;
import org.D7noun.model.Item.ItemState;
import org.omnifaces.util.Faces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 * Backing bean for Bill entities.
 * <p/>
 * This class provides CRUD functionality for all Bill entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class BillBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ItemBean itemBean;
	private List<Item> billItems = new ArrayList<Item>();

	/*
	 * Support creating and retrieving Bill entities
	 */

	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private Bill bill;

	public Bill getBill() {
		return this.bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	@Inject
	private Conversation conversation;

	@PersistenceContext(unitName = "yaqoot-pu", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public String create() {

		this.conversation.begin();
		this.conversation.setTimeout(1800000L);
		return "create?faces-redirect=true";
	}

	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}

		if (this.id == null) {
			this.bill = this.example;
		} else {
			this.bill = findById(getId());
			billItems = itemBean.getAllItemsByBillId(getId());
		}
	}

	public Bill findById(Long id) {

		return this.entityManager.find(Bill.class, id);
	}

	/*
	 * Support updating and deleting Bill entities
	 */

	public String update() {
		this.conversation.end();

		try {
			if (this.id == null) {
				this.entityManager.persist(this.bill);
				return "search?faces-redirect=true";
			} else {
				this.entityManager.merge(this.bill);
				return "create?faces-redirect=true&id=" + this.bill.getId();
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	public String delete() {
		this.conversation.end();

		try {
			Bill deletableEntity = findById(getId());
			this.entityManager.remove(deletableEntity);
			this.entityManager.flush();
			return "search?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	/*
	 * Support searching Bill entities with pagination
	 */

	private int page;
	private long count;
	private List<Bill> pageItems;

	private Bill example = new Bill();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public Bill getExample() {
		return this.example;
	}

	public void setExample(Bill example) {
		this.example = example;
	}

	public String search() {
		this.page = 0;
		return null;
	}

	public void paginate() {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<Bill> root = countCriteria.from(Bill.class);
		countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
		this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

		// Populate this.pageItems

		CriteriaQuery<Bill> criteria = builder.createQuery(Bill.class);
		root = criteria.from(Bill.class);
		TypedQuery<Bill> query = this.entityManager.createQuery(criteria.select(root).where(getSearchPredicates(root)));
		query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
		this.pageItems = query.getResultList();
	}

	private Predicate[] getSearchPredicates(Root<Bill> root) {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		double totalPrice = this.example.getTotalPrice();
		Date purchaseDate = this.example.getPurchaseDate();
		Factory factory = this.example.getFactory();

		if (totalPrice != 0) {
			predicatesList.add(builder.equal(root.get("totalPrice"), totalPrice));
		}

		if (purchaseDate != null) {
			predicatesList.add(builder.equal(root.get("purchaseDate"), purchaseDate));
		}

		if (factory != null) {
			predicatesList.add(builder.equal(root.get("factory"), factory));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}

	public List<Bill> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	/*
	 * Support listing and POSTing back Bill entities (e.g. from inside an
	 * HtmlSelectOneMenu)
	 */

	public List<Bill> getAll() {

		CriteriaQuery<Bill> criteria = this.entityManager.getCriteriaBuilder().createQuery(Bill.class);
		return this.entityManager.createQuery(criteria.select(criteria.from(Bill.class))).getResultList();
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private Bill add = new Bill();

	public Bill getAdd() {
		return this.add;
	}

	public Bill getAdded() {
		Bill added = this.add;
		this.add = new Bill();
		return added;
	}

	/**
	 * 
	 * Start Attachments
	 * 
	 */

	public void handleFileUpload(FileUploadEvent event) {
		try {

			UploadedFile uploadedFile = event.getFile();

			BillAttachment attachment = new BillAttachment();
			attachment.setBill(bill);
			attachment.setFileName(uploadedFile.getFileName());
			attachment.setSize(uploadedFile.getSize());
			attachment.setContentType(uploadedFile.getContentType());
			attachment.setData(uploadedFile.getContents());
			bill.getBillAttachments().add(attachment);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("BillBean: handleFileUpload");
		}
	}

	public void download(BillAttachment attachment) {
		try {
			Faces.sendFile(attachment.getData(), attachment.getFileName(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@EJB
	private BillAttachmentFacade attachmentFacade;
	private BillAttachment attachmentFordelete;

	public void selectForDelete(BillAttachment attachment) {
		attachmentFordelete = attachment;
	}

	public void deleteAttachment() {
		try {
			bill.getBillAttachments().remove(attachmentFordelete);
			attachmentFacade.removeBillAttachmentFromTable(bill, attachmentFordelete);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public StreamedContent getPicture() {
		FacesContext context = FacesContext.getCurrentInstance();

		if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
			return new DefaultStreamedContent();
		} else {
			String attachmentId = context.getExternalContext().getRequestParameterMap().get("attachmentId");
			if (attachmentId != null) {
				BillAttachment attachmentItem = attachmentFacade.findById(Long.parseLong(attachmentId));
				StreamedContent streamedContent = new DefaultStreamedContent(
						new ByteArrayInputStream(attachmentItem.getData()));
				return streamedContent;
			}
		}
		return new DefaultStreamedContent();
	}

	/**
	 * @return the billItems
	 */
	public List<Item> getBillItems() {
		return billItems;
	}

	/**
	 * @param billItems
	 *            the billItems to set
	 */
	public void setBillItems(List<Item> billItems) {
		this.billItems = billItems;
	}

	/**
	 * 
	 * End Attachments
	 * 
	 */

	/**
	 * 
	 * Add new Item
	 * 
	 */

	private Item newItem;

	public void createNewItem() {
		newItem = new Item();
		newItem.setBill(bill);
		newItem.setItemState(ItemState.Available);
	}

	public void saveNewItem() {
		try {
			billItems.add(newItem);
			this.entityManager.persist(newItem);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: saveNewItem");
		}
	}

	/**
	 * @return the newItem
	 */
	public Item getNewItem() {
		return newItem;
	}

	/**
	 * @param newItem
	 *            the newItem to set
	 */
	public void setNewItem(Item newItem) {
		this.newItem = newItem;
	}

}
