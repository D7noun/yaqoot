package org.D7noun.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.D7noun.facade.ItemAttachmentFacade;
import org.D7noun.facade.SettingsFacade;
import org.D7noun.model.Factory;
import org.D7noun.model.Item;
import org.D7noun.model.ItemAttachment;
import org.D7noun.model.SellingCard;
import org.D7noun.model.Settings;
import org.D7noun.model.Item.ItemState;
import org.omnifaces.util.Faces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 * Backing bean for Item entities.
 * <p/>
 * This class provides CRUD functionality for all Item entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class ItemBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private SettingsFacade settingsFacade;

	/*
	 * Support creating and retrieving Item entities
	 */

	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private Item item;

	public Item getItem() {
		return this.item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	@Inject
	private Conversation conversation;

	@PersistenceContext(unitName = "yaqoot-pu", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public String create() {

		this.conversation.begin();
		this.conversation.setTimeout(1800000L);
		return "create?faces-redirect=true";
	}

	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}

		if (this.id == null) {
			this.item = this.example;
		} else {
			this.item = findById(getId());
		}
	}

	public Item findById(Long id) {

		return this.entityManager.find(Item.class, id);
	}

	/*
	 * Support updating and deleting Item entities
	 */

	public String update() {
		this.conversation.end();

		try {
			if (this.id == null) {
				this.entityManager.persist(this.item);
				return "search?faces-redirect=true";
			} else {
				this.entityManager.merge(this.item);
				return "create?faces-redirect=true&id=" + this.item.getId();
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	public String delete() {
		this.conversation.end();

		try {
			Item deletableEntity = findById(getId());
			SellingCard sellingCard = deletableEntity.getSellingCard();
			if (sellingCard != null) {
				sellingCard.getItems().remove(deletableEntity);
				deletableEntity.setSellingCard(null);
				this.entityManager.merge(sellingCard);
			}
			this.entityManager.remove(deletableEntity);
			this.entityManager.flush();
			return "search?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	/*
	 * Support searching Item entities with pagination
	 */

	private int page;
	private long count;
	private List<Item> pageItems;

	private Item example = new Item();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public Item getExample() {
		return this.example;
	}

	public void setExample(Item example) {
		this.example = example;
	}

	public String search() {
		this.page = 0;
		return null;
	}

	public void paginate() {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<Item> root = countCriteria.from(Item.class);
		countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
		this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

		// Populate this.pageItems

		CriteriaQuery<Item> criteria = builder.createQuery(Item.class);
		root = criteria.from(Item.class);
		TypedQuery<Item> query = this.entityManager.createQuery(criteria.select(root).where(getSearchPredicates(root)));
		query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
		this.pageItems = query.getResultList();
	}

	private Predicate[] getSearchPredicates(Root<Item> root) {

		CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		String barcode = this.example.getBarcode();
		if (barcode != null && !"".equals(barcode)) {
			predicatesList
					.add(builder.like(builder.lower(root.<String>get("barcode")), '%' + barcode.toLowerCase() + '%'));
		}
		String types = this.example.getTypes();
		if (types != null && !"".equals(types)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("types")), '%' + types.toLowerCase() + '%'));
		}
		String stockLocation = this.example.getStockLocation();
		if (stockLocation != null && !"".equals(stockLocation)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("stockLocation")),
					'%' + stockLocation.toLowerCase() + '%'));
		}
		ItemState itemState = this.example.getItemState();
		if (itemState != null && !"".equals(itemState)) {
			predicatesList.add(builder.equal(root.get("itemState"), itemState));
		}
		Factory factory = this.example.getFactory();
		if (factory != null) {
			predicatesList.add(builder.equal(root.get("factory"), factory));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}

	public List<Item> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	/*
	 * Support listing and POSTing back Item entities (e.g. from inside an
	 * HtmlSelectOneMenu)
	 */

	public List<Item> getAll() {

		CriteriaQuery<Item> criteria = this.entityManager.getCriteriaBuilder().createQuery(Item.class);
		return this.entityManager.createQuery(criteria.select(criteria.from(Item.class))).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Item> getAllAvailableItems() {
		try {
			Query query = this.entityManager.createNamedQuery(Item.getAllItemsByState, Item.class);
			query.setParameter("itemState", ItemState.Available);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: getAllAvailableItems");
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<Item> getAllSoldItems() {
		try {
			Query query = this.entityManager.createNamedQuery(Item.getAllItemsByState, Item.class);
			query.setParameter("itemState", ItemState.Sold);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: getAllSoldItems");
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<Item> getAllItemsByBillId(Long billId) {
		try {
			Query query = this.entityManager.createNamedQuery(Item.getAllItemsByBillId, Item.class);
			query.setParameter("billId", billId);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: getAllItemsByBillId");
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<Item> getAllItemsByBuyerId(Long buyerId) {
		try {
			Query query = this.entityManager.createNamedQuery(Item.getAllItemsByBuyerId, Item.class);
			query.setParameter("buyerId", buyerId);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: getAllItemsByBuyerId");
		}
		return null;
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private Item add = new Item();

	public Item getAdd() {
		return this.add;
	}

	public Item getAdded() {
		Item added = this.add;
		this.add = new Item();
		return added;
	}

	/**
	 * 
	 * Start Attachments
	 * 
	 */

	public void handleFileUpload(FileUploadEvent event) {
		try {

			UploadedFile uploadedFile = event.getFile();

			ItemAttachment attachment = new ItemAttachment();
			attachment.setItem(item);
			attachment.setFileName(uploadedFile.getFileName());
			attachment.setSize(uploadedFile.getSize());
			attachment.setContentType(uploadedFile.getContentType());
			attachment.setData(uploadedFile.getContents());
			item.getItemAttachments().add(attachment);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("ItemBean: handleFileUpload");
		}
	}

	public void download(ItemAttachment attachment) {
		try {
			Faces.sendFile(attachment.getData(), attachment.getFileName(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@EJB
	private ItemAttachmentFacade attachmentFacade;
	private ItemAttachment attachmentFordelete;

	public void selectForDelete(ItemAttachment attachment) {
		attachmentFordelete = attachment;
	}

	public void deleteAttachment() {
		try {
			item.getItemAttachments().remove(attachmentFordelete);
			attachmentFacade.removeItemAttachmentFromTable(item, attachmentFordelete);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public StreamedContent getPicture() {
		FacesContext context = FacesContext.getCurrentInstance();

		if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
			return new DefaultStreamedContent();
		} else {
			String attachmentId = context.getExternalContext().getRequestParameterMap().get("attachmentId");
			if (attachmentId != null) {
				ItemAttachment attachmentItem = attachmentFacade.findById(Long.parseLong(attachmentId));
				StreamedContent streamedContent = new DefaultStreamedContent(
						new ByteArrayInputStream(attachmentItem.getData()));
				return streamedContent;
			}
		}
		return new DefaultStreamedContent();
	}

	/**
	 * 
	 * End Attachments
	 * 
	 */

	public List<String> getAllTypes() {
		try {
			List<String> allTypes = new ArrayList<String>();

			Settings settingTypes = settingsFacade.getValues("types");

			if (settingTypes != null) {
				allTypes.addAll(Arrays.asList(settingTypes.getValue().split("\\s*,\\s*")));
			}
			return allTypes;
		} catch (Exception e) {
			System.err.println("D7noun: getAllTypes");
			e.printStackTrace();
		}
		return new ArrayList<>();
	}
}
