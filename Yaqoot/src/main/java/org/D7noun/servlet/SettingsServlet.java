package org.D7noun.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.D7noun.facade.SettingsFacade;
import org.D7noun.util.CommonUtility;

import com.google.gson.Gson;

/**
 * Servlet implementation class SettingsServlet
 */
@WebServlet("/SettingsServlet")
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private SettingsFacade settingsFacade;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SettingsServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			Gson gson = new Gson();
			String settingValue = settingsFacade.getValues("types").getValue();
			if (settingValue == null) {
				settingValue = "";
			}
			List<String> settingsList = new ArrayList<String>();
			settingsList = CommonUtility.getListFromString(settingValue);

			response.getWriter().write(gson.toJson(settingsList));
		} catch (Exception e) {
			System.err.println("D7noun: settingsServlet");
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
