package org.D7noun.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@NamedQueries({
		@NamedQuery(name = Item.getAllItemsByState, query = "SELECT i FROM Item i where i.itemState = :itemState"),
		@NamedQuery(name = Item.getAllItemsByBillId, query = "SELECT i FROM Item i where i.bill.id = :billId"),
		@NamedQuery(name = Item.getAllItemsByBuyerId, query = "SELECT i from Item i where i.sellingCard.buyer.id = :buyerId") })
@Entity
public class Item implements Serializable {

	public static final String getAllItemsByState = "Item.getAllItemsByState";
	public static final String getAllItemsByBillId = "Item.getAllItemsByBillId";
	public static final String getAllItemsByBuyerId = "Item.getAllItemsByBuyerId";

	public enum ItemState {
		Available, Not_Available, Sold, Damaged, Maintenance
	}

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	@Version
	@Column(name = "version")
	private int version;

	@Column
	private String barcode;

	@Column
	private String types;

	@Column
	private double actualPrice;

	@Column
	private double sellingPrice;

	@Column
	private double discountPrice;

	@Column
	private double retailPrice;

	@Column
	private String color;

	@Column
	private String shape;

	@Column
	private String stockLocation;

	@Column
	@Enumerated(EnumType.STRING)
	private ItemState itemState;

	@ManyToOne
	private Factory factory;

	@ManyToOne
	private SellingCard sellingCard;

	@ManyToOne
	private Bill bill;

	@OneToMany(mappedBy = "item", cascade = CascadeType.MERGE, orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<ItemAttachment> itemAttachments = new HashSet<ItemAttachment>();

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	/**
	 * @return the barcode
	 */
	public String getBarcode() {
		return barcode;
	}

	/**
	 * @param barcode
	 *            the barcode to set
	 */
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	/**
	 * @return the types
	 */
	public String getTypes() {
		return types;
	}

	/**
	 * @param types
	 *            the types to set
	 */
	public void setTypes(String types) {
		this.types = types;
	}

	/**
	 * @return the actualPrice
	 */
	public double getActualPrice() {
		return actualPrice;
	}

	/**
	 * @param actualPrice
	 *            the actualPrice to set
	 */
	public void setActualPrice(double actualPrice) {
		this.actualPrice = actualPrice;
	}

	/**
	 * @return the sellingPrice
	 */
	public double getSellingPrice() {
		return sellingPrice;
	}

	/**
	 * @param sellingPrice
	 *            the sellingPrice to set
	 */
	public void setSellingPrice(double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	/**
	 * @return the discountPrice
	 */
	public double getDiscountPrice() {
		return discountPrice;
	}

	/**
	 * @param discountPrice
	 *            the discountPrice to set
	 */
	public void setDiscountPrice(double discountPrice) {
		this.discountPrice = discountPrice;
	}

	/**
	 * @return the retailPrice
	 */
	public double getRetailPrice() {
		return retailPrice;
	}

	/**
	 * @param retailPrice
	 *            the retailPrice to set
	 */
	public void setRetailPrice(double retailPrice) {
		this.retailPrice = retailPrice;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color
	 *            the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the shape
	 */
	public String getShape() {
		return shape;
	}

	/**
	 * @param shape
	 *            the shape to set
	 */
	public void setShape(String shape) {
		this.shape = shape;
	}

	/**
	 * @return the stockLocation
	 */
	public String getStockLocation() {
		return stockLocation;
	}

	/**
	 * @param stockLocation
	 *            the stockLocation to set
	 */
	public void setStockLocation(String stockLocation) {
		this.stockLocation = stockLocation;
	}

	/**
	 * @return the itemState
	 */
	public ItemState getItemState() {
		return itemState;
	}

	/**
	 * @param itemState
	 *            the itemState to set
	 */
	public void setItemState(ItemState itemState) {
		this.itemState = itemState;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "item [id=" + id + ", version=" + version + ", barcode=" + barcode + ", types=" + types
				+ ", actualPrice=" + actualPrice + ", sellingPrice=" + sellingPrice + ", discountPrice=" + discountPrice
				+ ", retailPrice=" + retailPrice + ", color=" + color + ", shape=" + shape + ", stockLocation="
				+ stockLocation + ", itemState=" + itemState + "]";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (Double.doubleToLongBits(actualPrice) != Double.doubleToLongBits(other.actualPrice))
			return false;
		if (barcode == null) {
			if (other.barcode != null)
				return false;
		} else if (!barcode.equals(other.barcode))
			return false;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (Double.doubleToLongBits(discountPrice) != Double.doubleToLongBits(other.discountPrice))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (itemState != other.itemState)
			return false;
		if (Double.doubleToLongBits(retailPrice) != Double.doubleToLongBits(other.retailPrice))
			return false;
		if (Double.doubleToLongBits(sellingPrice) != Double.doubleToLongBits(other.sellingPrice))
			return false;
		if (shape == null) {
			if (other.shape != null)
				return false;
		} else if (!shape.equals(other.shape))
			return false;
		if (stockLocation == null) {
			if (other.stockLocation != null)
				return false;
		} else if (!stockLocation.equals(other.stockLocation))
			return false;
		if (types == null) {
			if (other.types != null)
				return false;
		} else if (!types.equals(other.types))
			return false;
		if (version != other.version)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(actualPrice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((barcode == null) ? 0 : barcode.hashCode());
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		temp = Double.doubleToLongBits(discountPrice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((itemState == null) ? 0 : itemState.hashCode());
		temp = Double.doubleToLongBits(retailPrice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(sellingPrice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((shape == null) ? 0 : shape.hashCode());
		result = prime * result + ((stockLocation == null) ? 0 : stockLocation.hashCode());
		result = prime * result + ((types == null) ? 0 : types.hashCode());
		result = prime * result + version;
		return result;
	}

	/**
	 * @return the sellingCard
	 */
	public SellingCard getSellingCard() {
		return sellingCard;
	}

	/**
	 * @param sellingCard
	 *            the sellingCard to set
	 */
	public void setSellingCard(SellingCard sellingCard) {
		this.sellingCard = sellingCard;
	}

	/**
	 * @return the itemAttachments
	 */
	public Set<ItemAttachment> getItemAttachments() {
		return itemAttachments;
	}

	/**
	 * @param itemAttachments
	 *            the itemAttachments to set
	 */
	public void setItemAttachments(Set<ItemAttachment> itemAttachments) {
		this.itemAttachments = itemAttachments;
	}

	/**
	 * @return the factory
	 */
	public Factory getFactory() {
		return factory;
	}

	/**
	 * @param factory
	 *            the factory to set
	 */
	public void setFactory(Factory factory) {
		this.factory = factory;
	}

	public boolean locked() {
		if (itemState == ItemState.Sold) {
			return true;
		}
		return false;
	}

	public String display() {
		String s = "";
		if (barcode != null) {
			s += barcode;
		} else {
			s += "No Barcode";
		}

		s += " / ";
		if (types != null) {
			s += types;
		} else {
			s += "No Types";
		}

		s += " / ";
		if (color != null) {
			s += color;
		} else {
			s += "No Color";
		}

		s += " / ";
		if (shape != null) {
			s += shape;
		} else {
			s += "No Shape";
		}

		return s;
	}

	/**
	 * @return the bill
	 */
	public Bill getBill() {
		return bill;
	}

	/**
	 * @param bill
	 *            the bill to set
	 */
	public void setBill(Bill bill) {
		this.bill = bill;
	}

}