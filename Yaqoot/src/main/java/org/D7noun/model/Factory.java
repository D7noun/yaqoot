package org.D7noun.model;

import javax.persistence.Entity;
import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.Version;

@Entity
public class Factory implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	@Version
	@Column(name = "version")
	private int version;

	@Column
	private String name;

	@Column
	private String phoneNumber;

	@Column
	private String address;

	@Column
	private String email;

	@Column
	private String contact;

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Factory)) {
			return false;
		}
		Factory other = (Factory) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (name != null && !name.trim().isEmpty())
			result += "name: " + name;
		if (phoneNumber != null && !phoneNumber.trim().isEmpty())
			result += ", phoneNumber: " + phoneNumber;
		if (address != null && !address.trim().isEmpty())
			result += ", address: " + address;
		if (email != null && !email.trim().isEmpty())
			result += ", email: " + email;
		if (contact != null && !contact.trim().isEmpty())
			result += ", contact: " + contact;
		return result;
	}

	public String display() {
		String s = "";
		if (name != null || !name.equals("")) {
			s += name;
		} else {
			s += "No Name Factory";
		}
		s += " / ";
		if (phoneNumber != null || !phoneNumber.equals("")) {
			s += phoneNumber;
		} else {
			s += "No PhoneNumber Factory";
		}
		return s;
	}
}