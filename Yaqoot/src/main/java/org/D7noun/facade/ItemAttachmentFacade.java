package org.D7noun.facade;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.D7noun.model.Item;
import org.D7noun.model.ItemAttachment;

@Stateful
@Local
public class ItemAttachmentFacade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@PersistenceContext(unitName = "yaqoot-pu", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public ItemAttachment save(ItemAttachment attachment) {
		return entityManager.merge(attachment);
	}

	public ItemAttachment findById(long attachmentItemId) {
		try {
			Query query = getEntityManager().createNamedQuery(ItemAttachment.findById, ItemAttachment.class);
			query.setParameter(1, attachmentItemId);
			return (ItemAttachment) query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: findById");
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<ItemAttachment> findALl() {
		try {
			Query query = getEntityManager().createNamedQuery(ItemAttachment.getAttacmentsByItemId,
					ItemAttachment.class);
			return query.getResultList();
		} catch (Exception e) {
			System.err.println("D7noun: getAllValues");
			e.printStackTrace();
		}
		return null;
	}

	public void removeItemAttachmentFromTable(Item item, ItemAttachment deletedAttachment) {
		try {
			deletedAttachment.setItem(null);
			this.entityManager.merge(item);
			this.entityManager.remove(this.entityManager.contains(deletedAttachment) ? deletedAttachment
					: this.entityManager.merge(deletedAttachment));
			this.entityManager.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * D7noun
	 * 
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
