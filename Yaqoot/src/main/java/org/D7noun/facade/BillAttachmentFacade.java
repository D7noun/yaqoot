package org.D7noun.facade;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.D7noun.model.Bill;
import org.D7noun.model.BillAttachment;

@Stateful
@Local
public class BillAttachmentFacade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@PersistenceContext(unitName = "yaqoot-pu", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public BillAttachment save(BillAttachment attachment) {
		return entityManager.merge(attachment);
	}

	public BillAttachment findById(long attachmentItemId) {
		try {
			Query query = getEntityManager().createNamedQuery(BillAttachment.findById, BillAttachment.class);
			query.setParameter(1, attachmentItemId);
			return (BillAttachment) query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: findById");
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<BillAttachment> findALl() {
		try {
			Query query = getEntityManager().createNamedQuery(BillAttachment.getAttacmentsByBillId,
					BillAttachment.class);
			return query.getResultList();
		} catch (Exception e) {
			System.err.println("D7noun: getAllValues");
			e.printStackTrace();
		}
		return null;
	}

	public void removeBillAttachmentFromTable(Bill bill, BillAttachment deletedAttachment) {
		try {
			deletedAttachment.setBill(null);
			this.entityManager.merge(bill);
			this.entityManager.remove(this.entityManager.contains(deletedAttachment) ? deletedAttachment
					: this.entityManager.merge(deletedAttachment));
			this.entityManager.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * D7noun
	 * 
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
