$(document).ready(function() {
	initTypes();
});

function initTypes() {
	var options = [];
	var items = [];

	$.ajax({
		url : "/yaqoot/SettingsServlet",
		data : {
			type : "types"
		},
		type : "GET",
		success : function(data, status, object) {

			var json = JSON.parse(data);

			$.each(json, function(i, val) {
				options[i] = {
					name : val
				}
			})

			$('#itemBeanItemTypes').selectize(
					{
						delimiter : ',',
						persist : false,
						valueField : 'name',
						labelField : 'name',
						searchField : 'name',
						options : options,
						create : function(input) {
							return {
								value : input,
								text : input
							}
						},
						render : {
							item : function(item, escape) {
								return '<div>'
										+ (item.name ? '<span class="name">'
												+ escape(item.name) + '</span>'
												: '') + '</div>';
							},
							option : function(item, escape) {
								var label = item.name;
								return '<div>' + '<span class="label">'
										+ escape(label) + '</span></div>';
							}
						},
					});

		},
		failure : function(data, status, object) {
			console.debug('failure');
		}

	});
}
